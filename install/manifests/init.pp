class install{
	
	# Miscellaneous packages.	
	$misc_packages = ['vim-enhanced','telnet','zip','unzip','git','screen','libssh2','libssh2-devel', 'gcc', 'gcc-c++', 'autoconf', 	'automake']
	
	package { $misc_packages: ensure => latest }
	
	class {'stdlib':}
	
	class {'apache':}
		
	apache::vhost { 'myMpwar.dev':
		port          => '80',
		docroot       => '/web/',
		docroot_owner => 'vagrant',
		docroot_group => 'vagrant',
	}
	
	apache::vhost { 'myMpwar.prod':
	      port          => '80',
	      docroot       => '/web/production/',
	      docroot_owner => 'vagrant',
	      docroot_group => 'vagrant',
	}
	
	class {'::mysql::server':
		root_password    => 'root',
	  	override_options => { 'mysqld' => { 'max_connections' => '1024' } }
	}
	
	mysql_database { 'mympwar':
	  	ensure  => 'present',
	  	charset => 'latin1',
	  	collate => 'latin1_swedish_ci',
	}
	
	class {'memcached':}
	
	class {'generator':}
	
	class {'yum':}
	
	#PHP
	$php_version = '55'
	
	# remi_php55 requires the remi repo as well
	if $php_version == '55' {
	  $yum_repo = 'remi-php55'
	  include ::yum::repo::remi_php55
	}
	# remi_php56 requires the remi repo as well
	elsif $php_version == '56' {
	  $yum_repo = 'remi-php56'
	  ::yum::managed_yumrepo { 'remi-php56':
	    descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.6',
	    mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php56/mirror',
	    enabled        => 1,
	    gpgcheck       => 1,
	    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
	    gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
	    priority       => 1,
	  }
	}
	# version 5.4
	elsif $php_version == '54' {
	  $yum_repo = 'remi'
	  include ::yum::repo::remi
	}
	
	class { 'php':
	  version => 'latest',
	  require => Yumrepo[$yum_repo]
	}
	
	php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap' ]: }

}