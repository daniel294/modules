class generator {
	file { '/web/index.php':
		ensure => file,
		owner => "root",
		group => "root",
		mode => 755,
		replace => true,
		source => "puppet:///modules/generator/centos.php",
	}
	file { '/web/info.php':
		ensure => file,
		owner => "root",
		group => "root",
		mode => 755,
		replace => true,
		source => "puppet:///modules/generator/project1.php",
	}
}
